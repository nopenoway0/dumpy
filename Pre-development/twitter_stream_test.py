import tweepy
import codecs
import sys
import time


#in your twitter_keys.txt file, make sure you have the access token on first line, then access_token_secret on second line, then consumer_token on third line, and consumer_secret on fourth line in txt file.

with open("twitter_keys.txt") as key_file:
	data = key_file.readlines()
data = [x.strip() for x in data]

access_token = str(data[0])
access_token_secret = str(data[1])
consumer_token = str(data[2])
consumer_secret = str(data[3])


output_file = 'streamtest1.txt'


#Listener to print incoming tweets from twitter.
class StreamListener(tweepy.StreamListener): 

	def on_status(self, status):
		with open(output_file, 'w', encoding = "utf-8") as outfile:
			outfile.write(str(status))
		#print(status)

	def on_error(self, status):
		
		print(status)
		if status == 420:

			return False



if __name__ == '__main__':

	stream_listener = StreamListener()
	auth = tweepy.OAuthHandler(consumer_token, consumer_secret)
	auth.set_access_token(access_token, access_token_secret)
	stream = tweepy.Stream(auth, stream_listener)


	stream.filter(track=['Bitcoin', 'Litecoin', 'Ethereum'])
	#stream.sample()
